<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Services\Translations\TranslationLoad;
    use App\Services\Translations\TranslationDelete;
    use App\Services\Translations\TranslationNew;
    use App\Services\Translations\TranslationSave;
    use App\Services\GitRepository\GitRepository;

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Redirect;
    use Illuminate\Support\Facades\Lang;

    class TranslationController extends Controller
    {

        /**
         * @var TranslationLoad
         */
        protected $translation;

        /**
         * List of installed languages
         *
         * @var array
         */
        protected $languages;

        /**
         * Constructor
         *
         * @param TranslationLoad $trans
         */
        public function __construct(TranslationLoad $trans)
        {
            $this->translation = $trans;
            $this->languages = $trans->languages();
        }

        /**
         * Display a listing of the resource.
         *
         * @param string $language Language code using IETF language tag
         * @return Response
         */
        public function index($language = NULL)
        {
            if ($language === NULL && !in_array(Lang::getLocale(), $this->languages)) {
                $language = $this->languages[0];
            } elseif ($language === NULL && in_array(Lang::getLocale(), $this->languages)) {
                $language = Lang::getLocale();
            } elseif (!in_array($language, $this->languages)) {
                $language = $this->languages[0];
            }

            return view('admin.translation.index', [
                'translations' => $this->translation->get($language),
                'language'     => $language,
                'languages'    => $this->languages,
                'title'        => 'Tłumaczenie',
                'menu'         => 'translation',
            ]);
        }

        public function change(Request $req) {
            return $this->index($req->input('language-select'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $git = new GitRepository;
            $git->set('caouecs', 'Laravel-lang');

            return view('admin.translation.create', [
                'title'     => 'Dodaj nowe tłumaczenie',
                'menu'      => 'translation',
                'languages' => $git->get('baseDir', FALSE)
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param Request $req
         * @param TranslationNew $trans
         * @return Response
         */
        public function store(Request $req, TranslationNew $trans)
        {
            $this->validate($req, [
                'trans.language-tag'    => 'required',
                'trans.language-origin' => 'required',
            ]);

            $trans->get($req->input('trans'))->save();

            return Redirect::route('settingTranslationLocale', $req->input('trans.language-tag'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param Request $req
         * @param TranslationSave $trans
         * @param string $language Language code using IETF language tag
         * @return Response
         */
        public function update(Request $req, TranslationSave $trans, $language)
        {
            $trans->get([$language => $req->input('trans')])->set($language)->save();

            return Redirect::route('settingTranslationLocale', $language)
                ->with('status', 'Success');
        }

        /**
         * Display confirm window about deleting the resource
         *
         * @param string $language Language tag
         * @return Response
         */
        public function deleteConfirm($language)
        {
            return view('admin.translation.delete', [
                'language' => $language,
                'title'    => 'Usuń język',
                'menu'     => 'translation',
            ]);
        }

        /**
         * Delete the specified resource from storage
         *
         * @param Request $req
         * @param TranslationDelete $trans
         * @param string $language Language tag
         * @return Response
         */
        public function delete(Request $req, TranslationDelete $trans, $language)
        {
            if ($req->input('trans.delete') == 'TRUE') {
                $trans->get($language)->delete();
            }

            return Redirect::route('settingTranslation');
        }

        public function visible(Request $req) {
            $defaultFiles = $req->session()->get('translationHideDefaultFiles', FALSE);

            if($defaultFiles === FALSE) {
                $req->session()->put('translationHideDefaultFiles', TRUE);
            } else {
                $req->session()->put('translationHideDefaultFiles', FALSE);
            }

            return Redirect::route('settingTranslation');
        }

    }
