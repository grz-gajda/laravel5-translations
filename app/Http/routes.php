<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/translation/', ['uses' => 'TranslationController@index','as' => 'settingTranslation']);
Route::post('/translation/', ['uses' => 'TranslationController@change', 'as' => 'changeTranslationLocale']);
Route::put('/translation/', ['uses' => 'TranslationController@visible', 'as' => 'changeTranslationVisible']);
Route::get('/translation/show/{locale}', ['uses' => 'TranslationController@index', 'as' => 'settingTranslationLocale']);
Route::put('/translation/show/{locale}', ['uses' => 'TranslationController@update', 'as' => 'updateTranslationLocale']);
Route::get('/translation/add', ['uses' => 'TranslationController@create', 'as' => 'createTranslation']);
Route::post('/translation/add', ['uses' => 'TranslationController@store', 'as' => 'storeTranslation']);
Route::get('/translation/delete/{locale}', ['uses' => 'TranslationController@deleteConfirm', 'as' => 'deleteTranslationConfirm']);
Route::post('/translation/delete/{locale}', ['uses' => 'TranslationController@delete', 'as' => 'deleteTranslationLocale']);