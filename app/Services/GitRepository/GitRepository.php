<?php

    namespace App\Services\GitRepository;

    use App\Services\cURL\cURL;
    use Illuminate\Support\Facades\File;
    use League\Flysystem\Exception;

    class GitRepository extends GitApi implements GitRepositoryInterface
    {

        /**
         * Store an username of GitHub user
         *
         * @var string
         */
        private $gitUsername;

        /**
         * Store an repository name of GitHub user
         *
         * @var string
         */
        private $gitRepository;

        /**
         * Assign arguments to class variables
         *
         * @param string $gitUsername GitHub username
         * @param string $gitRepository GitHub repository
         */
        public function set($gitUsername, $gitRepository)
        {
            $this->gitUsername = $gitUsername;
            $this->gitRepository = $gitRepository;
        }

        /**
         * Get response from GitHub API
         *
         * @param string $type Response
         * @param bool $displayUrl Attach URL to response
         * @param bool|string $customUrl Request to GitHub API
         * @return array
         */
        public function get($type, $displayUrl, $customUrl = NULL)
        {
            switch ($type) {
                case 'baseDir':
                    $url = 'https://api.github.com/repos/' . $this->gitUsername . '/' . $this->gitRepository . '/contents/';

                    return $this->parse($this->getContent($url), 'dir', $displayUrl);

                case 'customDir':
                    return $this->parseInDir($this->getContent($customUrl));

                case 'customFile':
                    return $this->parseFile($this->getContent($customUrl));
            }
        }

        /**
         * Parse GitHub API response
         *
         * @param $response GitHub API Json response
         * @param string $lookFor Directory or files
         * @param bool $getUrl Return URL to content or not
         * @return array
         * @throws \Exception
         */
        protected function parse($response, $lookFor = 'dir', $getUrl = FALSE)
        {
            $return = [];

            if ($lookFor !== 'dir' && $lookFor !== 'file')
                throw new \Exception('Poszukiwany element musi być plikiem albo folderem');

            foreach ($response as $element) {
                if ($lookFor === 'dir') {
                    if ($element->type === $lookFor) {
                        $return[] = $this->parseAssign($element, $getUrl);
                    }
                } elseif ($lookFor === 'file') {
                    if ($element->type === $lookFor) {
                        $return[] = $this->parseAssign($element, $getUrl);
                    }
                }
            }

            return $return;
        }

        /**
         * Return a part of $element
         *
         * @param object $element
         * @param bool $url Return URL to content or not
         * @return array
         */
        protected function parseAssign($element, $url)
        {
            if ($url === TRUE) {
                return [
                    'name' => $element->name,
                    'url'  => $element->git_url,
                ];
            } else {
                return [
                    'name' => $element->name,
                ];
            }
        }

        /**
         * Parse GitHub API response
         *
         * @param object $response
         * @return array
         */
        protected function parseInDir($response) {
            $return = [];

            foreach($response->tree as $element) {
                $return[$element->path] = $element->url;
            }

            return $return;
        }

        /**
         * Get content from GitHub file
         *
         * @param object $response
         * @return string
         */
        protected function parseFile($response) {
            return base64_decode($response->content);
        }


    }