<?php

    namespace App\Services\GitRepository;

    interface GitRepositoryInterface
    {

        public function set($gitUsername, $gitRepository);

        public function get($type, $url);

    }