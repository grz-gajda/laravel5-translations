<?php

    namespace App\Services\GitRepository;

    abstract class GitApi
    {

        /**
         * Creates a stream context
         *
         * @return resource
         */
        final protected function addUserAgent()
        {
            return stream_context_create([
                'http' => [
                    'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                ],
            ]);
        }

        /**
         * Return an array from Json response
         *
         * @param string $url GitHub Api url
         * @return array
         * @throws \Exception
         */
        final protected function getContent($url)
        {
            if (!is_string($url)) throw new \Exception();

            return json_decode(file_get_contents($url, TRUE, $this->addUserAgent()));
        }

    }