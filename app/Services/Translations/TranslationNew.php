<?php

    namespace App\Services\Translations;

    use Illuminate\Support\Facades\Lang;
    use Illuminate\Support\Facades\File;
    use App\Services\GitRepository\GitRepository;

    class TranslationNew extends TranslationLoad
    {

        /**
         * Store a language tag of a newly created language
         *
         * @var string
         */
        protected $languageCode = "";

        /**
         * Store a language tag of copied language
         *
         * @var string
         */
        protected $languageOrigin = "";

        /**
         * Store files from GitHub API
         *
         * @var array
         */
        protected $gitFiles = [];

        /**
         * Constructor
         */
        public function __construct()
        {
            $this->getLanguages();
        }

        /**
         * Set class variables, get list of files
         *
         * @param null|string $language Language tag
         * @return $this
         * @throws \Exception
         */
        public function get($language = NULL)
        {
            if ($this->languageExists($language['language-tag']) === FALSE) {
                $this->languageCode = $language['language-tag'];
                $this->languageOrigin = $language['language-origin'];

                $this->gitGetFiles($language);
            } else {
                throw new \Exception("Wrong argument passed to method get in TranslationNew class.");
            }

            return $this;
        }

        protected function gitGetFiles($language)
        {

            $git = new GitRepository();
            $git->set('caouecs', 'Laravel-lang');

            foreach($git->get('baseDir', TRUE) as $languages) {
                if($languages['name'] === $language['language-origin']) {
                    $this->gitFiles = $git->get('customDir', TRUE, $languages['url']);
                }
            }

            return $this;
        }

        /**
         * Create a new directory for language
         *
         * @return $this
         */
        protected function make()
        {
            File::makeDirectory(base_path() . "/resources/lang/" . strtolower($this->languageCode), 0775);

            return $this;
        }

        /**
         * Copy language php files
         *
         * Copy language php files from one directory
         * to a second one.
         *
         * @return $this
         */
        protected function copy()
        {
            $folderUrl = base_path() . "/resources/lang/" . $this->languageCode . "/";

            foreach($this->gitFiles as $filename => $url) {
                $git = new GitRepository();
                $git->set('caouecs', 'Laravel-lang');

                $content = $git->get('customFile', TRUE, $url);
                File::put($folderUrl . $filename, $content);
            }

            return $this;
        }

        /**
         * Make a directory and copy the files.
         */
        public function save()
        {
            $this->make()->copy();
        }

    }