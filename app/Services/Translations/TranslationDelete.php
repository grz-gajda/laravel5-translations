<?php

    namespace App\Services\Translations;

    use Illuminate\Support\Facades\File;

    class TranslationDelete extends TranslationLoad
    {

        /**
         * Language tag of deleted directory
         *
         * @var string
         */
        protected $deleteDir = "";

        /**
         * Assign language tag to class variable
         *
         * @param null|string $language Language tag
         * @return $this
         * @throws \Exception
         */
        public function get($language = NULL)
        {
            if ($language === NULL) {
                throw new \Exception("Parametr \"\$language\" jest pusty.");
            } else {
                $this->getLanguages()->setLanguages($language)->getFiles();
                $this->deleteDir = $language;
            }

            return $this;
        }

        /**
         * Remove an entire directory
         *
         * Remove an entire directory of specific
         * language. If failed, throws an exception.
         *
         * @return $this
         * @throws \Exception
         */
        public function delete()
        {
            if($this->languageExists($this->deleteDir) === TRUE)
            {
                if(File::deleteDirectory(base_path() . "/resources/lang/" . $this->deleteDir . "/") === FALSE) {
                    $path = base_path() . "/resources/lang/" . $this->deleteDir;
                    throw new \Exception("Nie można usunąć folderu $path");
                } else {
                    return $this;
                }
            } else {
                throw new \Exception('Język ' . $this->deleteDir . ' nie istnieje.');
            }
        }

    }