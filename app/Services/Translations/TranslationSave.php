<?php

    namespace App\Services\Translations;

    use Illuminate\Support\Facades\File;

    class TranslationSave extends TranslationLoad
    {

        /**
         * Array of inputs from the form
         *
         * @var array
         */
        protected $get = [];

        /**
         * Merged array of $get and $translation
         *
         * @var array
         */
        protected $newTranslation = [];

        /**
         * Get values and save to class variable.
         *
         * Get values like an parameter, flush its
         * from empty values and assign to class variable.
         *
         * @param array $input Request input
         * @return $this
         */
        public function get($input = [])
        {
            $this->get = $this->flush($input);

            return $this;
        }

        /**
         * Prepare the output array of files
         *
         * Prepare the output array of files
         * of specific language passed in argument.
         *
         * @param string $language Language code IETF language tag
         * @return $this
         */
        public function set($language)
        {
            $this->getLanguages()->setLanguages($language)->getFiles()->render();

            return $this;
        }

        /**
         * Flush empty values from array
         *
         * Recursive look for the empty values
         * in array and children arrays. Empty values
         * are removed.
         *
         * @param array $array Pass array which has to be flushed
         * @return mixed
         */
        protected function flush(&$array)
        {
            foreach ($array as $key => $item) {
                is_array($item) && $array [ $key ] = $this->flush($item);
                if (empty ($array [ $key ]))
                    unset ($array [ $key ]);
            }

            return $array;
        }

        /**
         * Merge file' translation and user' translation
         *
         * Method merging translations from the file with
         * translation from form submit by user.
         *
         * @return $this
         */
        protected function merge()
        {
            $this->newTranslation = array_replace_recursive($this->translation, $this->get);

            return $this;
        }

        /**
         * Translate array to string.
         *
         * Translate array to string using loop
         * foreach. If value is array, use 'prepareSubArray'
         * recursively.
         *
         * @param array $arrays Translation's array
         * @return string
         */
        protected function prepare($arrays = [])
        {
            $start = "<?php\n";
            $result = [];

            foreach ($arrays as $key => $value) {
                if (is_string($value)) {
                    $result[] = "\"$key\" => \"$value\",";
                } elseif (is_array($value)) {
                    $result[] = $this->prepareSubArray($key, $value);
                }
            }

            $string = "return [";
            foreach ($result as $str) {
                $string .= "\n\t$str";
            }
            $string .= "\n];";

            return $start . $string;
        }

        /**
         * Translate array to string recursively
         *
         * @param string $key Key of row
         * @param string|array $value Value of row
         * @param bool|TRUE $finish Add "]," to end of string
         * @return string
         */
        protected function prepareSubArray($key, $value, $finish = TRUE)
        {
            $preparedResult = "\"$key\" => [\n";
            foreach ($value as $subKey => $subValue) {
                if (is_array($subValue)) {
                    $preparedResult = $this->prepareSubArray($subKey, $subValue, FALSE);
                } else {
                    $preparedResult .= "\t\t\"$subKey\" => \"$subValue\",\n";
                }
            }
            if ($finish === TRUE) $preparedResult .= "\n],";

            return $preparedResult;
        }

        /**
         * Save an array to file
         *
         * Save an array to file with
         * return statement. Array must be prepared
         * earlier.
         *
         * @return $this
         */
        public function save()
        {
            $this->merge();

            foreach ($this->newTranslation as $language => $files) {
                foreach ($files as $file => $array) {
                    $path = base_path() . "/resources/lang/$language/$file.php";
                    File::put($path, $this->prepare($array));
                }
            }

            return $this;
        }
    }