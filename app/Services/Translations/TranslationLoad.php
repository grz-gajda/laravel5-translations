<?php

    namespace App\Services\Translations;

    use Illuminate\Support\Facades\Lang;
    use Illuminate\Support\Facades\File;

    class TranslationLoad
    {

        /**
         * List of app's languages
         *
         * @var array
         */
        protected $languages = [];

        /**
         * List of languages specific by user '
         *
         * @var array
         */
        protected $usedLanguages = [];

        /**
         * List of loaded files
         *
         * @var array
         */
        protected $files = [];

        /**
         * List of translation strings
         *
         * @var array
         */
        protected $translation = [];

        /**
         * Prepare and return the $translation array
         *
         * Array $translation contains all strings from
         * PHP files with strings translation.
         *
         * @return array
         */
        public function get($language = NULL)
        {
            if($language === NULL) $language = Lang::getLocale();
            $this->getLanguages()->setLanguages($language)->getFiles()->render();

            return $this->translation;
        }

        /**
         * Return list of all languages
         *
         * Return list of all directories
         * in folder 'resources/lang'. Each folder
         * is another language installed in app.
         *
         * @return array
         */
        public function languages() {
            if(empty($this->languages)) {
                $this->getLanguages();
            }

            return $this->languages;
        }

        /**
         * Get list of languages installed in app
         *
         * Get list of directories placed in
         * 'resources/lang' folder. Each directory is
         * an another language.
         *
         * @return $this
         */
        protected function getLanguages()
        {
            $langs = File::directories(base_path() . '/resources/lang/');

            foreach ($langs as $lang) {
                $this->languages[] = basename($lang);
            }

            return $this;
        }

        /**
         * Set languages for getFiles() method
         *
         * @param string $languages Language code using IETF language tag
         * @return $this
         */
        protected function setLanguages($languages)
        {
            if (empty($languages) || $languages === []) {
                $this->usedLanguages = [
                    $this->languages[array_search($languages, $this->languages)]
                ];
            } else {
                if ($this->languageExists($languages)) $this->usedLanguages[] = $languages;
            }

            return $this;
        }

        /**
         * Return list of files
         *
         * Return list of language files. First
         * method checking what languages are specified.
         *
         * @return $this
         */
        protected function getFiles()
        {
            foreach ($this->usedLanguages as $language) {
                $this->files[ $language ] = File::allFiles(base_path() . '/resources/lang/' . $language);
            }

            return $this;
        }

        /**
         * Prepare the output array.
         *
         * @return $this
         */
        protected function render()
        {
            foreach($this->files as $locale => $language) {
                foreach($language as $file) {
                    $this->translation[$locale][File::name($file)] = File::getRequire($file);
                }
            }

            return $this;
        }

        /**
         * Check if language exists in app
         *
         * Looking in languages array for specific language,
         * if exists then return true. Unless return false.
         *
         * @param string $lang Language code IETF language tag
         * @return bool
         */
        protected function languageExists($lang)
        {
            if (in_array($lang, $this->languages)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }