# Laravel 5 Translations #

### Jak to działa? ###

Został stworzony specjalny panel administracji gdzie można wyświetlić wszystkie zainstalowane języki (foldery zlokalizowane w `resources/lang/`). Każdy język powinien zawierać cztery podstawowe pliki: `auth.php`, `pagination.php`, `passwords.php`, `validation.php`. 

### Co można edytować? ###

Można edytować wszystkie pliki, kod pobiera wszystkie stringi do drugiego (chyba?) zagnieżdżenia. 

### Instalacja języków ###

Można zainstalować nowe języki, języki pobierane są z [tego repozytorium](https://github.com/caouecs/Laravel-lang)