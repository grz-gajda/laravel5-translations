@extends('admin.layout')

@section('content')
    <h2>Dodaj nowe tłumaczenie</h2>
    <div class="form-field">
        <form class="content form" method="post" action="{{ route('storeTranslation') }}">
            <input type="hidden" name="_method" value="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            @if(count($errors) > 0)
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-warning" role="alert">
                            <p>Wszystkie pola muszą być uzupełnione by stworzyć nowy język.</p>
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="list-row">
                        <label for="trans[language-tag]">Kod językowy nowego języka (IETF language tag)</label>
                        <div class="form-group">
                            <input type="text" name="trans[language-tag]" class="form-control" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="list-row">
                        <label for="trans[language-origin]">Pliki źródłowe dla nowego języka</label>
                        <div class="form-group">
                            <select name="trans[language-origin]" class="form-control" required>
                                @foreach($languages as $language)
                                    <option value="{{ $language['name'] }}">{{ strtoupper($language['name']) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-button col-md-12">
                    <input type="submit" class="btn btn-primary" value="Zapisz nowy język">
                </div>
            </div>
        </form>
    </div>
@stop