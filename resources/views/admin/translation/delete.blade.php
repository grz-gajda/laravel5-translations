@extends('admin.layout')

@section('content')
    <h2>Usuń tłumaczenie</h2>
    <div class="form-field row">
        <div class="col-md-12">
            <p>Czy na pewno chcesz usunąć folder oraz pliki zawierające tłumaczenie dla strony w języku <strong>{{ $language }}</strong>?</p>
            <div class="alert alert-info" role="alert">
                <p>Akcja, którą zamierzasz wykonać, jest nieodwracalna. Jesteś pewien, że chcesz usunąć lokalizację?</p>
            </div>
            <form method="POST" action="{{ route('deleteTranslationLocale', $language) }}">
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="trans[delete]" value="TRUE">
                <input type="submit" class="btn btn-primary" value="Tak, usuń pliki lokalizacji">
                <a href="{{ route('settingTranslation') }}" class="btn btn-primary">Nie, wróć do poprzedniej strony</a>
            </form>
        </div>
    </div>
@stop