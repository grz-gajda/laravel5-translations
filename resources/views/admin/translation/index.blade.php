@extends('admin.layout')

@section('content')
    <h2>Tłumaczenie</h2>
    {{--  Language settings --}}
    <div class="form-field">
        <div class="row">
            {{-- Language tag --}}
            <div class="col-md-12">
                <h2>Przeglądany język: <strong>{{ strtoupper($language) }}</strong></h2>
            </div>
        </div>
        <div class="row">
            {{-- Change the language --}}
            <div class="col-md-4">
                <p>Wybrany język</p>
                <form method="POST" action="{{ route('changeTranslationLocale') }}">
                    {{-- Form protection --}}
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-6" id="language-change-select">
                            {{-- Select option --}}
                            <select name="language-select" class="form-control">
                                @foreach($languages as $lang)
                                    @if($lang === $language)
                                        <option value="{{ $lang }}" selected>{{ strtoupper($lang) }}</option>
                                    @else
                                        <option value="{{ $lang }}">{{ strtoupper($lang) }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6" id="language-change-submit">
                            {{-- Submit option --}}
                            <input type="submit" class="btn btn-primary" value="Zmień język">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-7">
                <p>Ustawienia widoku</p>
                <form action="{{ route('changeTranslationVisible') }}" method="POST">
                    {{-- Form protection --}}
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @if(session('translationHideDefaultFiles') === TRUE)
                        <input type="submit" class="btn btn-primary" value="Pokaż pliki domyślne">
                    @else
                        <input type="submit" class="btn btn-primary" value="Schowaj pliki domyślne">
                    @endif
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p>Ustawienia językowe</p>
                <form method="POST" action="{{ route('updateTranslationLocale', $language) }}">
                    {{-- Form protection --}}
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {{-- Save changes --}}
                    <input type="submit" class="btn btn-primary" value="Zapisz zmiany">
                    <a class="btn btn-primary" href="{{ route('createTranslation') }}">Dodaj język</a>
                    {{-- Remove translation --}}
                    <a class="btn btn-primary" href="{{ route('deleteTranslationConfirm', $language) }}">Usuń język {{ $language }}</a>
            </div>
        </div>
        @if(session('status'))
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success" role="alert">
                        <p>Aktualizacja języka została przeprowadzona prawidłowo.</p>
                    </div>
                </div>
            </div>
        @endif
    </div>

    @foreach($translations[$language] as $filename => $translation)
        @if(in_array($filename, ['auth', 'pagination', 'passwords', 'validation']))
            @if(session('translationHideDefaultFiles') === TRUE)
                <div class="form-field core-file hidden-lg">
            @else
                <div class="form-field core-file">
            @endif
        @else
            <div class="form-field custom-file">
        @endif
            @if(in_array($filename, ['auth', 'pagination', 'passwords', 'validation']))
                <h2>Plik {{ $filename }}.php - plik domyślny</h2>
            @else
                <h2>Plik {{ $filename }}.php</h2>
            @endif
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th style="width: 16%;">Klucz</th>
                        <th style="width: 42%;">Tłumaczenie {{ strtoupper($language) }}</th>
                        <th style="width: 42%;">Nowe tłumaczenie</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($translation as $key => $string)
                    @if(is_array($string))
                        @foreach($string as $insideKey => $insideString)
                            @if(is_array($insideString))
                                @foreach($insideString as $insInsideKey => $insInsideString)
                                    <tr>
                                        <td>{{ $insInsideKey }}</td>
                                        <td>{{ $insInsideString }}</td>
                                        <td><input  class="form-control"
                                                    name="trans[{{$filename}}][{{$key}}][{{$insideKey}}][{{$insInsideKey}}]"
                                                    type="text"
                                                    placeholder="{{ $insInsideString }}"></td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td>{{ $insideKey }}</td>
                                    <td>{{ $insideString }}</td>
                                    <td><input  class="form-control"
                                                name="trans[{{$filename}}][{{$key}}][{{$insideKey}}]"
                                                type="text"
                                                placeholder="{{ $insideString }}"></td>
                                </tr>
                            @endif
                        @endforeach
                    @else
                        <tr>
                            <td>{{ $key }}</td>
                            <td>{{ $string }}</td>
                            <td><input  class="form-control"
                                        name="trans[{{$filename}}][{{$key}}]"
                                        type="text"
                                        placeholder="{{ $string }}"></td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    @endforeach

    <div class="form-field">
        <div class="row">
            <div class="col-md-12">
                <input type="submit" class="btn btn-primary" value="Zapisz zmiany">
            </div>
        </div>
    </div>
    </form>
@stop
