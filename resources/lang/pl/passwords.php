<?php
return [
	"password" => "Hasło musi mieć przynajmniej sześć znaków i zgadzać się z potwierdzeniem.",
	"user" => "Nie znaleziono użytkownika z takim adresem e-mail.",
	"token" => "Token resetowania hasła jest nieprawidłowy.",
	"sent" => "Przypomnienie hasła zostało wysłane!",
	"reset" => "Hasło zostało zresetowane!",
];